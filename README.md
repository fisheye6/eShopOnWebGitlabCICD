EShopOnWeb Gitlab CICD implementation


<a href="https://gitlab.com/fisheye6/eShopOnWebGitlabCICD/-/commits/main"><img alt="pipeline status" src="https://gitlab.com/fisheye6/eShopOnWebGitlabCICD/badges/main/pipeline.svg" /></a>



## TO DO

- Testing the application
- Building image
- Pushing the build docker image to gitlab artifact repo
- Provision a development environment with Terraform AWS
- Run some additional testing
- Provision a production environment with Terraform on AWS
- Use Hashicorp Consul for service discovery